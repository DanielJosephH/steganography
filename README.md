# Steganography

Hide/extract plain text inside a .PNG image

# Pre-requisites
--------------------------------------------------------------------
must have PIL library installed, this can be done using:

pip install pillow

# Usage
-------------------------------------------------------------------
Usage can be read from the program using:
python image-steg.py -h
to receive help and usage information

To run image-steg.py on a .PNG file image.PNG and a payload file payload.txt,
simply use:

python3 image-steg.py image.PNG payload.txt [-e]
if -e is specified than the program will extract the payload from the image and
store it in the payload text file.
