# This program can hide a 'payload' (the secret text) inside a .PNG file using
# Least-Significant-Bit Steganography. Or extract a payload if -e is specified.
import imghdr
import argparse
from PIL import Image
import sys
from itertools import cycle

DEBUG = False

# *** USAGE ***
# python image_steg file_path payload_path [-e] [-h]
# saves new file to new.png, or saves payload to the payload file if -e specfied
class StegIMG:

    # convert a char(s) to an 8-bit binary representation
    def char_to_binary(self, text):
        return ''.join('{:08b}'.format(ord(char), 'b') for char in text)

    # convert an 8-bit binary to a char representation
    def binary_to_char(self, text):
        return chr(int(text, 2))
        # return ''.join('{:08b}'.format(ord(char), 'b') for byte in text)

    # TO-DO
    def extract_payload(self):
        # the payload is repeated until the end of the file, it has the symbols
        # ***** before each repeat. Look for one of these segments to start reading.
        i = 0
        next_payload_char_bits = ['']
        for row in range(self.carrier_image.size[1]):
                for col in range(self.carrier_image.size[0]):
                    pixel = tuple(self.carrier_image.getpixel((col,row)))
                    for pixel_data in pixel:
                        # convert pixel data to binary and extract last bit to payload bit
                        bin_data_bits = list(bin(pixel_data))
                        next_payload_char_bits.append(bin_data_bits[-1])
                        i += 1
                        # 1-byte has been read, convert it to a char and add it to the payload text
                        if (i >= 8):
                            i = 0
                            self.payload_text += self.binary_to_char(''.join(next_payload_char_bits))
                            next_payload_char_bits.clear()

    def hide_payload(self):
        # calculate the size of the image and check it can hold the payload
        self.image_size = self.carrier_image.size[1] * self.carrier_image.size[0]
        # L mode images can store 1 bit per pixel without noticable change
        # must be large enough to hold headers and metadata
        if (self.image_size <= 2 * len(self.payload_text) * 8):
            print("Error: image is too small to hold payload")

        # create binary payload data, the asterisks show the message is starting
        # again, followed by the message
        self.payload = self.char_to_binary("*****") + self.char_to_binary(self.payload_text)

        # copy the payload to the least signifcant bits of the carrier image
        # repeat the payload until the end of the carrier is reached to prevent
        # the payload being lost due to cropping/ image edits
        payload_cycle = cycle(self.payload)
        next_payload_bit = next(payload_cycle)

        for row in range(self.carrier_image.size[1]):
                for col in range(self.carrier_image.size[0]):
                    pixel = tuple(self.carrier_image.getpixel((col,row)))
                    new_pixel = ()
                    for pixel_data in pixel:
                        # convert pixel data to binary and change last bit to payload bit
                        bin_data_bits = list(bin(pixel_data))
                        bin_data_bits[-1] = next_payload_bit
                        new_pixel_data = int(''.join(bin_data_bits), 2)
                        # iterate to next payload bit (will cycle at end)
                        next_payload_bit = next(payload_cycle)
                        new_pixel += (new_pixel_data,)
                    self.carrier_image.putpixel((col, row), new_pixel)


    def __init__(self, image_path = None, payload_path = None, extract = False):
        self.payload_path = payload_path
        self.payload_text = ''
        self.carrier_image_path = image_path
        self.carrier_image = None
        self.extract = extract
        self.image_type = None
        self.supported_file_types = ['png']

        # check user inputs are valid
        if (self.carrier_image_path is None or self.payload_path is None):
            print("INVALID INPUT: image_path or payload_path is not specified")
            sys.exit()

        # check file types are supported and can be opened
        try:
            self.image_type = imghdr.what(self.carrier_image_path)
            if (self.image_type not in self.supported_file_types):
                print("INVALID INPUT: image file type is not supported, use .PNG")
                sys.exit()

            # load a copy of the carrier image (keep original intact)
            self.carrier_image = Image.open(self.carrier_image_path).copy()
        except:
            print("ERROR: carrier file could not be read")
            sys.exit()
        try:
            if (self.payload_path[-4:] != ".txt"):

                print("INVALID INPUT: payload file type is not supported, use .txt")
                sys.exit()

            # copy all text from payload file
            f = open(self.payload_path, 'r+')
        except:
            print("ERROR: payload file could not be opened")
            sys.exit()

        if (self.extract):
            # copy the payload from the carrier image
            self.extract_payload()

            # save results to the payload file
            f.write(self.payload_text)
            f.close()
        else:
            # create the payload
            # copy the payload into the carrier_image
            self.payload_text = f.read()
            f.close()
            self.hide_payload()

            # save the image changes
            self.carrier_image.save(str('new.' + self.image_type), self.image_type)

def main():

    # parse command line arguments, should include a carrier file and a payload
    # text file.
    parser = argparse.ArgumentParser(description='Hide/Extract text inside a .PNG image.')
    parser.add_argument('carrier_path', help='a file path to the carrier file.')
    parser.add_argument('payload_path', help='the text file path to the location of the payload, or to save the payload in if -e specified.')
    parser.add_argument('-e', '--extract', dest='should_extract', default=False,
    action='store_true', help='specifies to extract the payload to the payload file.')

    args = parser.parse_args()

    StegIMG(args.carrier_path, args.payload_path, args.should_extract)

if __name__ == '__main__':
    main()
